import { Component } from "react";
import '../../App.css';
import FooterContent from "./footerContent/footerContent";
import FooterSocial from "./footerSocial/footerSocial";
class footer extends Component {
    render() {
        return (
            <>
                <footer class="section footer-classic context-dark bg-image">
                  <FooterContent/>
                  <FooterSocial/>
                </footer>
            </>
        )
    }
}
export default footer;