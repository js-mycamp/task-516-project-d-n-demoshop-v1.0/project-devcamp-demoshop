import { Component } from "react";
import '../../../App.css'

class footerContent extends Component{
    render(){
        return(
<>
<div class="container">
                        <div class="row row-30 footer-row">
                            <div class="col-md-4 col-xl-5">
                                <div class="pr-xl-4">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <p class="rights">
                                        <span>©&nbsp; </span>
                                        <span class="copyright-year">2018</span>
                                        <span>&nbsp;</span>
                                        <span>.&nbsp;</span>
                                        <span>All Rights Reserved.</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h5>Contacts</h5>
                                <dl class="contact-list">
                                    <dt>Address:</dt>
                                    <dd>Kolkata, West Bengal, India</dd>
                                </dl>
                                <dl class="contact-list">
                                    <dt>email:</dt>
                                    <dd>
                                        <a href="mailto:#">info@example.com</a>
                                    </dd>
                                </dl>
                                <dl class="contact-list">
                                    <dt>phones:</dt>
                                    <dd><a href="tel:#">+91 99999999</a>
                                        <span>or</span>
                                        <a href="tel:#">+91 11111111</a>
                                    </dd>
                                </dl>
                            </div>
                            <div class="col-md-4 col-xl-3">
                                <h5>Links</h5>
                                <ul class="nav-list" style={{content:"\F14F"}}>
                                    <li>
                                        <a href="#about">About</a>
                                    </li>
                                    <li>
                                        <a href="#projects">Projects</a>
                                    </li>
                                    <li>
                                        <a href="#blog">Blog</a>
                                    </li>
                                    <li>
                                        <a href="#contacts">Contacts</a>
                                    </li>
                                    <li>
                                        <a href="#pricing">Pricing</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
</>
        )
    }
}
export default footerContent;