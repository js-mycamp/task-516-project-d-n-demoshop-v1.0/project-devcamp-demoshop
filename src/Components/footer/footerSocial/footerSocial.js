import { Component } from "react";

class footerSocial extends Component {
    render(){
        return(
            <>
             <div class="row no-gutters social-container">
                        <div class="col">
                            <a class="social-inner" href="#facebook">
                                <span class="icon mdi mdi-facebook"></span>
                                <span>Facebook</span>
                            </a>
                        </div>
                        <div class="col">
                            <a class="social-inner" href="#instagram">
                                <span class="icon mdi mdi-instagram">
                                </span>
                                <span>instagram</span>
                            </a>
                        </div>
                        <div class="col">
                            <a class="social-inner" href="#twitter">
                                <span class="icon mdi mdi-twitter">
                                </span>
                                <span>twitter</span>
                            </a>
                        </div>
                        <div class="col">
                            <a class="social-inner" href="#google">
                                <span class="icon mdi mdi-youtube-play"></span>
                                <span>google</span>
                            </a>
                        </div>
                    </div>
            </>
        )
    }
}
export default footerSocial;