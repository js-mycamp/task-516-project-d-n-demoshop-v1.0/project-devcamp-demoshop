import { Component } from "react";
import img from "../../../assets/images/reactJs.png";
class headerLogo extends Component{
    render(){
        return(
            <>
             <div class="#">
                    <div class="row">
                        <div class="col-md-4 text-center mt-2">
                            <img src={img} alt="abc" width="70px" />
                        </div>
                        <div class="col-md-4 text-center">
                            <h3>React Store</h3>
                            <p>Demo App Shop24h v1.0</p>
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                 
                </div>
            </>
        )
    }
}
export default headerLogo