import { Component } from "react"


class headerNavbar extends Component {
    render() {
        return (
            <>
                <nav class="navbar navbar-expand-md bg-dark navbar-dark">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="collapsibleNavbar">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a aria-current="page" class="nav-link active" href="/">Home</a>
                            </li>
                        </ul>
                    </div>
                    <div></div>
                </nav>
            </>
        )
    }
}

export default headerNavbar