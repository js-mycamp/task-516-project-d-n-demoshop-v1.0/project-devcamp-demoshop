import { Component } from "react";
import HeaderLogo from "./headerLogo/headerLogo"
import HeaderNavbar from "./headerNavbar/headerNavbar";
class header extends Component {
    render() {
        return (
            <>
                < HeaderLogo />
                <div>
                    < HeaderNavbar />
                </div>
            </>
        )
    }
}
export default header