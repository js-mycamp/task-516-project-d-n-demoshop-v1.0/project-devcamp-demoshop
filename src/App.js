
import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import Header from './Components/header/header';
import Content from "./Components/content/content";
import Footer from "./Components/footer/footer"

function App() {
  return (
  <>
      < Header />
      < Content />
      < Footer />
  </>

  );
}

export default App;
